# Macros for SIP
# ~~~~~~~~~~~~~~
# Copyright (c) 2007, Simon Edwards <simon@simonzone.com>
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying COPYING-CMAKE-SCRIPTS file.
#
# SIP website: http://www.riverbankcomputing.co.uk/sip/index.php
#
# This file defines the following macros:
#
# ADD_SIP_PYTHON_MODULE (MODULE_NAME MODULE_SIP [library1, libaray2, ...])
#     Specifies a SIP file to be built into a Python module and installed.
#     MODULE_NAME is the name of Python module including any path name. (e.g.
#     os.sys, Foo.bar etc). MODULE_SIP the path and filename of the .sip file
#     to process and compile. libraryN are libraries that the Python module,
#     which is typically a shared library, should be linked to. The built
#     module will also be install into Python's site-packages directory.
#
# The behaviour of the ADD_SIP_PYTHON_MODULE macro can be controlled by a
# number of variables:
#
# SIP_INCLUDES - List of directories which SIP will scan through when looking
#     for included .sip files. (Corresponds to the -I option for SIP.)
#
# SIP_TAGS - List of tags to define when running SIP. (Corresponds to the -t
#     option for SIP.)
#
# SIP_CONCAT_PARTS - An integer which defines the number of parts the C++ code
#     of each module should be split into. Defaults to 8. (Corresponds to the
#     -j option for SIP.)
#
# SIP_DISABLE_FEATURES - List of feature names which should be disabled
#     running SIP. (Corresponds to the -x option for SIP.)
#
# SIP_EXTRA_OPTIONS - Extra command line options which should be passed on to
#     SIP.
# SIP_EXTRA_SOURCE_FILES - Extra source files that will be added to the target
#     library.
#

SET(SIP_INCLUDES)
SET(SIP_TAGS)
SET(SIP_CONCAT_PARTS 1)
SET(SIP_DISABLE_FEATURES)
SET(SIP_EXTRA_OPTIONS)

find_file(sip_generate "sip-generate.py" PATHS ${CMAKE_MODULE_PATH} NO_CMAKE_FIND_ROOT_PATH)
find_file(pyproject_toml "pyproject.toml.in" PATHS ${CMAKE_MODULE_PATH} NO_CMAKE_FIND_ROOT_PATH)

macro(add_sip_python_module MODULE_NAME MODULE_SIP)

    get_filename_component(module_name_toml ${MODULE_NAME} NAME_WE)

    set(module_srcs "${SIP_EXTRA_FILES_DEPEND}")

    set(EXTRA_LINK_LIBRARIES ${ARGN})

    if (SIP_MODULE)
        set(sip_name ${SIP_MODULE})
    else()
        set(sip_name "sip")
    endif()

    set(module_tags)
    foreach(_tag ${SIP_TAGS})
        string(APPEND module_tags "\"${_tag}\",")
    endforeach()
    set(module_tags "[${module_tags}]")

    set(sip_include_dirs)
    foreach(_inc ${SIP_INCLUDES})
        get_filename_component(_abs_inc ${_inc} ABSOLUTE)
        string(APPEND sip_include_dirs "\"${_abs_inc}\",")
    endforeach()
    set(sip_include_dirs "[${sip_include_dirs}]")

    string(REPLACE "." "/" _x ${MODULE_NAME})
    get_filename_component(_parent_module_path ${_x} PATH)
    get_filename_component(_child_module_name ${_x} NAME)
    get_filename_component(_module_path ${MODULE_SIP} PATH)

    set(CMAKE_CURRENT_SIP_OUTPUT_DIR "${CMAKE_CURRENT_BINARY_DIR}/sip_out")
    get_filename_component(_abs_module_sip ${MODULE_SIP} ABSOLUTE)
    configure_file( "${_abs_module_sip}" "${module_name_toml}.sip" COPYONLY )

    # We give this target a long logical target name.
    # (This is to avoid having the library name clash with any already
    # install library names. If that happens then cmake dependency
    # tracking get confused.)
    string(REPLACE "." "_" _logical_name ${MODULE_NAME})
    set(_logical_name "python_module_${_logical_name}")

    set(_sip_output_files)
    foreach(CONCAT_NUM RANGE 0 ${SIP_CONCAT_PARTS})
        if(${CONCAT_NUM} LESS ${SIP_CONCAT_PARTS})
            list(APPEND _sip_output_files
                ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip${_child_module_name}part${CONCAT_NUM}.cpp
            )
        endif( ${CONCAT_NUM} LESS ${SIP_CONCAT_PARTS})
    endforeach(CONCAT_NUM RANGE 0 ${SIP_CONCAT_PARTS})
    list(APPEND _sip_output_files
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sipAPI${_child_module_name}.h
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_array.h
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_array.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_core.h
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_core.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_descriptors.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_enum.h
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_enum.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip.h
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_int_convertors.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_object_map.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_threads.c
        ${CMAKE_CURRENT_SIP_OUTPUT_DIR}/${_child_module_name}/sip_voidptr.c
    )

    configure_file( ${pyproject_toml} ${CMAKE_CURRENT_BINARY_DIR}/pyproject.toml )

    add_custom_command(
        COMMAND
            ${CMAKE_COMMAND} -E echo "Generating SIP bindings for ${MODULE_NAME}..."
        COMMAND
            ${Python_EXECUTABLE}
            ${sip_generate}
            --build-dir ${CMAKE_CURRENT_SIP_OUTPUT_DIR}
            --target-dir ${CMAKE_INSTALL_PREFIX}/${PYTHON_SITE_PACKAGES_DIR}/${_parent_module_path}
            --concatenate ${SIP_CONCAT_PARTS}
        WORKING_DIRECTORY
            ${CMAKE_CURRENT_BINARY_DIR}
        DEPENDS
            ${CMAKE_CURRENT_BINARY_DIR}/pyproject.toml
        OUTPUT
            ${_sip_output_files}
    )

    if (WIN32 OR CYGWIN OR APPLE)
        add_library(${_logical_name} MODULE ${_sip_output_files} ${SIP_EXTRA_SOURCE_FILES} )
    else(WIN32 OR CYGWIN OR APPLE)
        add_library(${_logical_name} SHARED ${_sip_output_files} ${SIP_EXTRA_SOURCE_FILES} )
    endif(WIN32 OR CYGWIN OR APPLE)

    target_include_directories(${_logical_name} PRIVATE ${CMAKE_CURRENT_SIP_OUTPUT_DIR})
    target_link_libraries(${_logical_name} ${EXTRA_LINK_LIBRARIES})
    set_target_properties(${_logical_name} PROPERTIES PREFIX "" OUTPUT_NAME ${_child_module_name})

    if (MINGW)
        target_compile_definitions(${_logical_name} PRIVATE _hypot=hypot)
    elseif(NOT MSVC)
        target_compile_definitions(${_logical_name} PRIVATE SIP_PROTECTED_IS_PUBLIC)
        target_compile_definitions(${_logical_name} PRIVATE protected=public)
    endif(MINGW)

    if(MSVC)
        set_target_properties(${_logical_name} PROPERTIES PDB_NAME "sip_${_logical_name}")
    endif(MSVC)

    if (WIN32)
        set_target_properties(${_logical_name} PROPERTIES SUFFIX ".pyd")
    endif(WIN32)

    install(TARGETS ${_logical_name} DESTINATION "${CMAKE_INSTALL_PREFIX}/${PYTHON_SITE_PACKAGES_DIR}/${_parent_module_path}")
endmacro(add_sip_python_module)
